package benchmark

import (
	"database/sql"
	"fmt"
	"log"
	"time"
)

func (b *Config) Run(db *sql.DB) error {
	for _, query := range b.Queries {
		var duration time.Duration

		for i := 0; i < int(query.Count); i++ {
			startTime := time.Now()

			err := query.Run(db)
			if err != nil {
				log.Fatal(err)
			}

			duration += time.Since(startTime)
		}

		fmt.Printf("%s ran %d times and took %s\n", query.Name, query.Count,
			duration.String())
	}

	return nil
}

package benchmark

import (
	"database/sql"
	"log"
	"math/rand"
	"time"
)

type Parameter struct {
	Func string
	Min  int
	Max  int
	SQL  string

	data any
}

func (param *Parameter) Get(db *sql.DB) any {
	switch param.Func {
	case "RandomInt":
		return param.randomInt()

	case "RandomSQLID":
		return param.randomSQLID(db)
	}

	return nil
}

var randomIntDidSeed bool

func seedRand() {
	if randomIntDidSeed {
		randomIntDidSeed = true
		rand.Seed(time.Now().UnixNano())
	}
}

func (param *Parameter) randomInt() int {
	seedRand()

	return rand.Intn(param.Max-param.Min) + param.Min
}

func (param *Parameter) randomSQLID(db *sql.DB) int {
	seedRand()

	if param.data == nil {
		rows, err := db.Query(param.SQL)
		if err != nil {
			log.Fatal(err)
		}

		var ids []int

		for rows.Next() {
			var id int

			err := rows.Scan(&id)
			if err != nil {
				log.Fatal(err)
			}

			ids = append(ids, id)
		}

		param.data = ids
	}

	var ids []int

	switch typedValue := param.data.(type) {
	case []int:
		ids = typedValue
	}

	randIdx := rand.Intn(len(ids))

	return ids[randIdx]
}

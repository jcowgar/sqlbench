package benchmark

import (
	"database/sql"
	"fmt"
	"log"
	"strings"
)

type Query struct {
	Name       string
	SQL        string
	Tag        string
	Count      int32
	Parameters []Parameter

	useQuery bool
	useExec  bool
}

func (query *Query) Run(db *sql.DB) error {
	if !query.useQuery && !query.useExec {
		query.determineType()
	}

	if query.useQuery {
		return query.query(db)
	}

	return query.exec(db)
}

func (query *Query) determineType() {
	query.useQuery = strings.HasPrefix("SELECT", strings.ToUpper(query.SQL))
	query.useExec = !query.useQuery

	fmt.Printf("%+v\n", query)
}

func (query *Query) query(db *sql.DB) error {
	params, err := query.getParameters(db)
	if err != nil {
		log.Fatal(err)
	}

	rows, err := db.Query(query.SQL, params...)
	if err != nil {
		return err
	}

	var recordCount int

	for rows.Next() {
		recordCount += 1
	}

	return nil
}

func (query *Query) exec(db *sql.DB) error {
	params, err := query.getParameters(db)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(query.SQL, params...)

	return err
}

func (query *Query) getParameters(db *sql.DB) ([]any, error) {
	var parameters []any

	for index := range query.Parameters {
		param := query.Parameters[index]
		parameters = append(parameters, param.Get(db))
		query.Parameters[index] = param
	}

	return parameters, nil
}

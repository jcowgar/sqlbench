package benchmark

import "gopkg.in/yaml.v3"

type Config struct {
	URL     string
	Queries []Query
	Driver  string
}

func (b *Config) Parse(data []byte) error {
	return yaml.Unmarshal(data, b)
}

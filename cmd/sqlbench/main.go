package main

import (
	"database/sql"
	"log"
	"os"

	"codeberg.org/jcowgar/sqlbench/pkg/benchmark"

	_ "github.com/jackc/pgx/v5/stdlib"
)

func main() {
	data, err := os.ReadFile("./bench.yaml")
	if err != nil {
		log.Fatal(err)
	}

	var benchmark benchmark.Config

	if err := benchmark.Parse(data); err != nil {
		log.Fatal(err)
	}

	db, err := sql.Open(benchmark.Driver, benchmark.URL)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	err = benchmark.Run(db)
	if err != nil {
		log.Fatal(err)
	}
}

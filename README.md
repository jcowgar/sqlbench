SQLBench
========

Execute SQL statements capturing key performance metrics.

Ideas
-----

`sqlbench -tagged quick bench.yaml`


	url: postgres://user@host/db
	queries:
		- name: SELECT 1
		  sql: SELECT 1

		- name: Insert Person
		  sql: INSERT INTO person (age, first_name, last_name) VALUES ($1, $2, $3)
		  parameters:
		  	- func: RandomInteger
		  	- func: RandomFirstName
		  	- func: RandomLastName
